def findSumOfElements(array, target):

    # To check the hash value of number using dictionary which is empty
    array_dict = {}
    list_pairs=[]
    for index, value in enumerate(array):

    # subtratcing value from target sum and checking it is present as key in dictionary
        if target - value in array_dict:
            #Appending to list of all indexes whose sum is equal to target
            if array[array_dict.get(target - value)] != array[index]:
                list_pairs.append(str(array_dict.get(target - value))+":"+str(index))


    # inserting index of element in the dictionary as value
        array_dict[value] = index

    if len(list_pairs) == 0:
        return []
    else :
        index1 = int(list_pairs[0].split(":")[0])
        index2 = int(list_pairs[0].split(":")[1])
        return [array[index2],array[index1]]

# main method starting from here
if __name__ == '__main__':

    array = [1,5,3,4,-1,0,1]
    target = 9

    list_pairs=findSumOfElements(array, target)
    print(list_pairs)