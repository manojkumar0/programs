import math
num=200

# incase the number is even make it odd
if(num%2==0):
    print(2)
    num=num/2

# atmost the multipliers will be <= the square root of a number
for i in range(3,int(math.sqrt(num))+1,2):
    while(num%i==0):
        print(i)
        num=num/i

if num>2:
    print(num)