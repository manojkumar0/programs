# solving using min heap : O(n + klogn)

import heapq
from heapq import heappop
 
# Function to find the k'th smallest element in the
# list using min-heap
def find_kth_smallest(A, k):
    # transform the input list into a min-heap
    heapq.heapify(A)
    # pop from the min-heap exactly (k-1) times
    while k > 1:
        heappop(A)
        k = k - 1
    # return the root of min-heap
    return A[0]
 
if __name__ == '__main__':
    A = [7, 4, 6, 3, 9, 1]
    k = 3
    print("K'th smallest element in the list is", find_kth_smallest(A, k))

##################################################
# To find 3 largest and 3 smallest numbers
li1 = [6, 7, 9, 4, 3, 5, 8, 10, 1] 
# # using heapify() to convert list into heap 
heapq.heapify(li1) 
  
# # using nlargest to print 3 largest numbers 
# # prints 10, 9 and 8 
# print("The 3 largest numbers in list are : ",end="") 
print(heapq.nlargest(3, li1)) 
  
# # using nsmallest to print 3 smallest numbers 
# # prints 1, 3 and 4 
# print("The 3 smallest numbers in list are : ",end="") 
print(heapq.nsmallest(3, li1)) 
