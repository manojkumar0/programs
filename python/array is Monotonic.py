def isMonotonic(A): 

    l1=[A[i] <= A[i + 1] for i in range(len(A) - 1)]
    # all() function returns True if all items in an iterable are true, otherwise it returns False.
    print(all(l1))
    l2=[A[i] >= A[i + 1] for i in range(len(A) - 1)]
    print(all(l2))
    return (all(A[i] <= A[i + 1] for i in range(len(A) - 1)) or
            all(A[i] >= A[i + 1] for i in range(len(A) - 1))) 
  
# Driver program 
A = [6, 5, 4, 4] 
  
# Print required result 
print(isMonotonic(A)) 