def count(S, N):
    n = len(S)
    T = [[0] * (N + 1) for i in range(n + 1)]

    for i in range(n + 1):
        T[i][0] = 1

    for i in range(1, n + 1):
        for j in range(1, N + 1):
            if S[i - 1] > j:
                T[i][j] = T[i - 1][j]
            else:
                T[i][j] = T[i - 1][j] + T[i][j - S[i - 1]]
    return T[n][N]

if __name__ == '__main__':
# n coins of given denominations
    S = 1, 2, 3
# Total Change required
    N = 4
    print("Total number of ways to get desired change is", count(S, N))