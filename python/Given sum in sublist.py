# Function to print sublist having given sum using Hashing
def findSublist(A, sum):

    # insert (0, -1) pair into the set to handle the case when
    # sublist with given sum starts from index 0
    dict = {0: -1}

    # maintains sum of elements so far
    sum_so_far = 0

    # traverse the given list
    for i in range(len(A)):

        # update sum_so_far
        sum_so_far += A[i]

        # if (sum_so_far - sum) is seen before, we have found
        # the sublist with sum 'sum'
        if (sum_so_far - sum) in dict:
            print("Sublist found", (dict.get(sum_so_far - sum) + 1, i))
            

        # insert current sum with index into the dict
        dict[sum_so_far] = i
    print(dict)
    # {0: -1, 1: 0, 3: 1, 6: 2, 10: 3, 15: 4}

if __name__ == '__main__':

    # list of integers
    A = [1,2,3,4,5]
    sum = 9

    findSublist(A, sum)
