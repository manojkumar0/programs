# Function to find the size of largest square sub-matrix of 1's
# present in the given binary matrix
def findLargestSquare(M):

    # T[i][j] stores the size of maximum square
    # sub-matrix ending at M[i][j]
    T = [[0 for x in range(len(M[0]))] for y in range(len(M))] 
    print(T)
    # max stores the size of largest square sub-matrix of 1's
    max = 0

    # fill in bottom-up manner
    for i in range(len(M)):
        for j in range(len(M[0])):
            T[i][j] = M[i][j]

            # if we are not at the first row or first column and
            # current cell has value 1
            if i > 0 and j > 0 and M[i][j] == 1:
                # largest square sub-matrix ending at M[i][j] will be 1 plus
                # minimum of largest square sub-matrix ending at M[i][j-1],
                # M[i-1][j] and M[i-1][j-1]

                T[i][j] = min(T[i][j - 1], T[i - 1][j], T[i - 1][j - 1]) + 1

            # update maximum size found so far
            if max < T[i][j]:
                max = T[i][j]

 T->[[0, 0, 1, 0, 1, 1], 
     [0, 1, 1, 1, 0, 0], 
     [0, 0, 1, 2, 1, 1], 
     [1, 1, 0, 1, 2, 2], 
     [1, 2, 1, 1, 2, 3], 
     [1, 2, 0, 1, 2, 3], 
     [1, 0, 1, 1, 2, 3], 
     [1, 1, 1, 0, 1, 2]]
    # return size of largest square matrix
    return max


if __name__ == '__main__':

    # input matrix
    mat = [
        [0, 0, 1, 0, 1, 1],
        [0, 1, 1, 1, 0, 0],
        [0, 0, 1, 1, 1, 1],
        [1, 1, 0, 1, 1, 1],
        [1, 1, 1, 1, 1, 1],
        [1, 1, 0, 1, 1, 1],
        [1, 0, 1, 1, 1, 1],
        [1, 1, 1, 0, 1, 1]
    ]

    print("The size of largest square sub-matrix of 1's is", findLargestSquare(mat))
