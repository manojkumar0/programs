NO_OF_CHARS = 256

def canFormPalindrome(string):
	
	# Create a count array and initialize all 
	# values as 0
	count = [0 for i in range(NO_OF_CHARS)]

	# For each character in input strings,
	# increment count in the corresponding
	# count array
	for i in string:
		count[ord(i)] += 1

	# Count odd occurring characters
	odd = 0
	for i in range(NO_OF_CHARS):
        # num=200 num & 1 ==> 0 num=201 num & 1==>1 
        # 0 is false and 1 is true
		if (count[i] & 1):
			odd += 1

		if (odd > 1):
			return False

	# Return true if odd count is 0 or 1, 
	return True

# Driver program to test to print printDups
if(canFormPalindrome("geeksforgeeks")):
	print "Yes"
else:
	print "No"
if(canFormPalindrome("geeksogeeks")):
	print "Yes"
else:
	print "NO"


