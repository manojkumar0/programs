# Iterative function to search in given BST
def searchIterative(root, key):
# start with root node
    curr = root
# pointer to store parent node of current node
    parent = None

    while curr and curr.data != key:
         parent = curr
# if given key is less than the current node, go to left subtree else go to right subtree
        if key < curr.data:
            curr = curr.left
        else:
            curr = curr.right

# if key is present not in the key
    if curr is None:
        print("Key Not found")
        return

    if parent is None:
        print("The node with key", key, "is root node")
    elif key < parent.data:
        print("Given key is left node of node with key", parent.data)
    else:
    print("Given key is right node of node with key", parent.data)
