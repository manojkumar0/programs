package data_structures;
import java.util.*;

import javax.swing.text.html.MinimalHTMLWriter;

class Node
{
	int data;
	Node left;
	Node right;
	Node(int d)
	{
		data=d;
		left=null;
		right=null;
	}
}
public class Binary_search_tree {
 static Node root;
 boolean b1=false;boolean b2=false;
 static ArrayList al =new ArrayList();
 Binary_search_tree()
 {
   root=null;	 
 }
 public void in(int key)
 {
	root=insert(root,key);
 }
 public Node insert(Node root, int key)
 {
	 if(root==null)
	 {
		 Node n1=new Node(key);
		 return n1;
	 }
	 if(root.data>key)
	 {
		root.left= insert(root.left,key);
	 }
	 else if(root.data<key)
	 {
		root.right= insert(root.right,key);
	 }
	 return root;
 }
 public void inorder()
 {
	 inorder(root);
 }
 public void inorder(Node root)  /// we are doing inorder because it make the elements sorted
 {
	 if(root==null)
	 {
		 return ;
	 }
	 inorder(root.left);
	 {
		 al.add(root.data);
	 System.out.print(root.data+" ");
	 }
	 inorder(root.right);
 }
 public static int minimum_value(Node root)
 {
	 while(root.left!=null)
	 {
		 root=root.left;
	 }
	 return root.data;
 }
 public Node lcafinder(int n1,int n2)
 {
	 Node lca=LCA(root,n1,n2);
	 if(b1==true&&b2==true)
	 {
		 return lca;
	 }
	 else return  null;
 }
 public Node LCA(Node root,int n1,int n2)
 {
	 if(root==null)
	 {
		 return null;
	 }
	 if(root.data==n1)
	 {
		 b1=true;
		 return root;
	 }
	 if(root.data==n2)
	 {
		 b2=true;
		 return root;
	 }
	 Node left_lca=LCA(root.left,n1,n2);
	 Node right_lca=LCA(root.right,n1,n2);
		if(left_lca!=null&&right_lca!=null)
		{
			return root;
		}
		else 
		return (left_lca!=null)?left_lca:right_lca;
 }
 
	public static void main(String[] args) {
Binary_search_tree b= new Binary_search_tree();
b.in(51);
b.in(42);
b.in(121);
b.in(11);
b.in(8);
b.in(44);
System.out.print("inordr traversal of binary search tree------");
b.inorder();
System.out.println();
System.out.println("In arraylist form    "+al);
int minimum=minimum_value(root);
System.out.println("the minimum value is "+minimum);
Node lca=b.lcafinder(11, 44);
if(lca!=null)
{
	System.out.println(lca.data);
}
	}

}
