void deleteNode(ListNode* node) {
        //deleting node without head pointer
        int temp;
        ListNode* pre=node;

        while(node->next){
                pre = node;
                node->val = (node->next)->val;
                node=node->next;
        }

        // at last we will be getting two nodes with same data, So we will make it as null
        pre->next=NULL;
        free(node);
}
