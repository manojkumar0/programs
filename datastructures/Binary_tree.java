package data_structures;

class Node
{
	int data;
	Node left,right;
	Node(int d)
	{
		data=d;
		left=right=null;
	}
}
public class Binary_tree {
Node root;
/*Binary_tree(int key)
{
	root=new Node(key);
}*/
Binary_tree()
{
	root=null;
}

public void preorder(Node n)
{
	if(n==null)
	{
		return;
	}
	System.out.print(n.data+" ");
	preorder(n.left);
	preorder(n.right);
}

public void inorder(Node n)
{
	if(n==null)
	{
		return;
	}
	inorder(n.left);
	System.out.print(n.data+" ");
	inorder(n.right);
}

public void postorder(Node n)
{
	if(n==null)
	{
		return;
	}
	postorder(n.left);
	postorder(n.right);
	System.out.print(n.data+" ");
}

	public void callpreorder()
	{
		System.out.println();
		System.out.print("preorder list is ");
		preorder(root);
	}
	public void callinorder()
	{
		System.out.println();
		System.out.print("inorder list is  ");
		inorder(root);
	}
	public void callpostorder()
	{
		System.out.println();
		System.out.print("postorder list is "
				+ "");
		postorder(root);
	}
	public static void main(String[] args) {
Binary_tree b= new Binary_tree();
b.root=new Node(1);
b.root.left=new Node(2);
b.root.right=new Node(3);
b.root.left.left=new Node(4);
b.root.left.right=new Node(5);
b.callpreorder();
b.callinorder();
b.callpostorder();
	}

}
