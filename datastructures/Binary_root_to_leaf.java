package data_structures;

class Node
{
	int data;
	Node left;
	Node right;
	Node(int d)
	{
		data=d;
		right=null;left=null;
	}
}
public class Binary_root_to_leaf {
	
	Node root;
	public void opt(Node root)
	{
		int arr[]=new int[100];
		print(root,arr,0);
	}
	public void print(Node root,int arr[],int arrlength)
	{
		if(root==null)
		{
			return;
		}
		arr[arrlength]=root.data;
		arrlength++;
		if(root.left==null&&root.right==null)
		{
			printarray(arr,arrlength);
		}
		else
		{
			print(root.left,arr,arrlength);
			print(root.right,arr,arrlength);
		}
	}
	public void printarray(int arr[],int arrlength)
	{
		
		for(int i=0;i<arrlength;i++)
		{
			System.out.print(arr[i]+"->");
		}
		System.out.println();
	}
	public static void main(String[] args) {
		Binary_root_to_leaf tree=new Binary_root_to_leaf();
		tree.root = new Node(10);
        tree.root.left = new Node(8);
        tree.root.right = new Node(2);
        tree.root.left.left = new Node(3);
        tree.root.left.right = new Node(5);
        tree.root.right.left = new Node(2);
        tree.opt(tree.root);
	}
}
